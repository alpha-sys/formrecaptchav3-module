<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsMonologLogger;
use TheRealWorld\ToolsPlugin\Core\ToolsApi;

/**
* central Configuration for FormReCaptchaV3
*/
class FormReCaptchaV3Helper
{
    /**
    * the formelement-prefix
    * @var string
    */
    protected static $_aFormElementPrefix = [
        'user'       => 'invadr[oxuser__%s]',
        'pricealarm' => 'pa[%s]',
        'contact'    => 'editval[oxuser__%s]',
        'invite'     => 'editval[send_%s]'
    ];

    /**
    * the FrontendController with Forms for ReCaptchaV3s
    * @var string
    */
    protected static $_aFrontendController = [
        'AccountUserController'    => 'user',
        'ArticleDetails'           => 'pricealarm',
        'ArticleDetailsController' => 'pricealarm',
        'ContactController'        => 'contact',
        'InviteController'         => 'invite',
        'PriceAlarmController'     => 'pricealarm',
        'RegisterController'       => 'user',
        'SuggestController'        => 'invite',
        'UserComponent'            => 'user',
        'UserController'           => 'user'
    ];

    /**
    * the FrontendController with ReCaptchaV3 Checks
    * @var string
    */
    protected static $_aControllerWithReCaptchaV3 = [
        'ArticleDetailsController',
        'ContactController',
        'InviteController',
        'PriceAlarmController',
        'SuggestController',
        'UserComponent'
    ];

    /**
    * get the FrontendController with ReCaptchaV3 Checks
    *
    * @return array
    */
    public static function getFrontendControllerWithReCaptchaV3()
    {
        return self::$_aControllerWithReCaptchaV3;
    }

    /**
    * check Google ReCaptchaV3
    *
    * @param string $sFrontendController - FrontendController
    *
    * @return boolean
    */
    public static function checkReCaptchaV3($sFrontendController)
    {
        $bResult = false;
        $oConfig = Registry::getConfig();

        if ($oConfig->getConfigParam('bTRWFormReCaptchaV3UseReCaptchaV3')) {
            $sFormElementPrefix = self::$_aFrontendController[$sFrontendController];
            $bThrowExceptionOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowExceptionOnError');
            $bThrowMessageOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowMessageOnError');

            // check if request not valid ReCaptchaV3
            $bNotValidReCaptchaV3 = false;

            $sRecaptchaResult = ToolsApi::executeExternApiFileGetContents(
                'https://www.google.com/recaptcha/api/siteverify',
                [
                    'secret'   => $oConfig->getConfigParam('sTRWFormReCaptchaV3SecretKey'),
                    'response' => $oConfig->getRequestParameter('recaptcha_response')
                ]
            );
            $oRecaptchaResult = json_decode($sRecaptchaResult);

            // Take action based on the score returned
            // According to the documentation,
            // 1.0 is very likely a good interaction,
            // 0.0 is very likely a bot'.
            // For simplicity, I'm accepting all submissions
            // from any user with a score of 0.5 or above.
            if ($oRecaptchaResult->score <= floatval($oConfig->getConfigParam('sTRWFormReCaptchaV3Score'))) {
                $bNotValidReCaptchaV3 = true;
            }

            // collect IP & Form
            $aRequestData = [
                'controller' => $sFrontendController,
                'clientIP'   => (
                    $oConfig->getConfigParam('bTRWFormReCaptchaV3CollectIP') ?
                    $_SERVER['REMOTE_ADDR'] :
                    ''
                )
            ];

            // collect full request
            $aRequestParam = explode('[', self::$_aFormElementPrefix[$sFormElementPrefix]);
            $aRequestData = array_merge(
                $aRequestData,
                $oConfig->getRequestParameter($aRequestParam[0])
            );

            // Error Handling
            if (($bThrowExceptionOnError || $bThrowMessageOnError) && $bNotValidReCaptchaV3) {
                $bResult = true;

                if ($bThrowMessageOnError) {
                    Registry::getUtilsView()->addErrorToDisplay('ERROR_MESSAGE_RECAPTCHAV3_MESSAGE');
                }
                if ($bThrowExceptionOnError) {
                    self::_writeToLog(
                        'ERROR_MESSAGE_RECAPTCHAV3_EXCEPTION',
                        $aRequestData
                    );
                }
            }

            // Info Handling
            if (
                $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowExceptionOnUse') &&
                in_array(
                    $sFrontendController,
                    $oConfig->getConfigParam('aTRWFormReCaptchaV3UseMessageForFrontendController')
                )
            ) {
                self::_writeToLog(
                    'INFO_MESSAGE_RECAPTCHAV3_EXCEPTION',
                    $aRequestData,
                    'info'
                );
            }
        }
        return $bResult;
    }

    /**
    * split a Varname as array if nesseccary an return the requestvalue
    *
    * @param string $sVarName
    *
    * @return string
    */
    private static function _getRequestParameter($sVarName)
    {
        $sResult = '';
        $oConfig = Registry::getConfig();

        if (strpos($sVarName, '[')) {
            $aVarName = explode('[', $sVarName);
            $aRequestParams = $oConfig->getRequestParameter($aVarName[0]);
            $sVarValueName = trim($aVarName[1], ']');
            $sResult = $aRequestParams[$sVarValueName];
        } else {
            $sResult = $oConfig->getRequestParameter($sVarName);
        }
        return $sResult;
    }

    /**
    * write a Exeption Message to the log
    *
    * @param string $sMessage (oLang Variable)
    * @param mixed  $mData (Data for logging), would be converted to string
    * @param string $sLogLevel (e.g. error, info, warning ... see Monolog\Logger
    *
    * @return null
    */
    private static function _writeToLog($sMessage, $mData, $sLogLevel = 'error')
    {
        $oConfig = Registry::getConfig();

        $sData = '';
        if (is_array($mData)) {
            // help: build array to string without print_r
            $sData = str_replace('=', ':', http_build_query($mData, null, ','));
        }

        $oEx = oxNew(\OxidEsales\Eshop\Core\Exception\InputException::class);
        $oEx->setMessage(sprintf(
            Registry::getLang()->translateString($sMessage),
            $sData
        ));

        $oLogger = ToolsMonologLogger::getLogger('trwformrecaptchav3');

        call_user_func([$oLogger, $sLogLevel], $oEx->getMessage(), [$oEx]);

        return null;
    }
}
