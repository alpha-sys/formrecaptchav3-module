<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

/**
* Metadata version
*/
$sMetadataVersion = '2.1';

/**
* Module information
*/
$aModule = [
    'id' => 'trwformrecaptchav3',
    'title' => [
        'de' => 'the-real-world - Formular Google ReCaptcha V3 Schutz',
        'en' => 'the-real-world - Form Google ReCaptcha V3 Protection'
    ],
    'description' => [
        'de' => 'A Google-Recaptcha-V3-Module für öffentliche Formulare z.b. Kontaktformular in OXID6.',
        'en' => 'A Google-Recaptcha-V3-Module for public forms like contact-form in OXID6.'
    ],
    'thumbnail' => 'picture.png',
    'version' => '1.2',
    'author' => 'Mario Lorenz',
    'url' => 'https://www.the-real-world.de',
    'email' => 'mario_lorenz@the-real-world.de',
    'extend' => [
        // Component
        \OxidEsales\Eshop\Application\Component\UserComponent::class => \TheRealWorld\FormReCaptchaV3Module\Application\Component\UserComponent::class,
        // Frontend-Controller
        \OxidEsales\Eshop\Application\Controller\ArticleDetailsController::class => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\ArticleDetailsController::class,
        \OxidEsales\Eshop\Application\Controller\ContactController::class        => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\ContactController::class,
        \OxidEsales\Eshop\Application\Controller\InviteController::class         => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\InviteController::class,
        \OxidEsales\Eshop\Application\Controller\NewsletterController::class     => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\NewsletterController::class,
        \OxidEsales\Eshop\Application\Controller\PriceAlarmController::class     => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\PriceAlarmController::class,
        \OxidEsales\Eshop\Application\Controller\SuggestController::class        => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\SuggestController::class,
        // Backend-Controller
        \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration::class => \TheRealWorld\FormReCaptchaV3Module\Application\Controller\Admin\ModuleConfiguration::class
    ],
    'blocks' => [
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var_type_arr',
            'file'     => 'Application/views/blocks/admin_module_config_var_type_arr.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/contact.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/privatesales/invite.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/pricealarm.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/suggest.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/fieldset/user_billing.tpl',
            'block'    => 'form_user_billing_country',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'flow',
            'template' => 'form/newsletter.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/contact.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/privatesales/invite.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/pricealarm.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/suggest.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/fieldset/user_billing.tpl',
            'block'    => 'form_user_billing_country',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ],
        [
            'theme'    => 'wave',
            'template' => 'form/newsletter.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl'
        ]
    ],
    'settings' => [
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3UseReCaptchaV3',
            'type'  => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowMessageOnError',
            'type'  => 'bool',
            'value' => 'true'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowExceptionOnError',
            'type'  => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowMessageOnError',
            'type'  => 'bool',
            'value' => 'true'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowExceptionOnUse',
            'type'  => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3CollectIP',
            'type'  => 'bool',
            'value' => 'false'
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'aTRWFormReCaptchaV3UseMessageForFrontendController',
            'type'  => 'arr',
            'value' => ''
        ],
        [
            'group' => 'trwformrecaptchav3googleapi',
            'name'  => 'sTRWFormReCaptchaV3WebsiteKey',
            'type'  => 'str',
            'value' => ''
        ],
        [
            'group' => 'trwformrecaptchav3googleapi',
            'name'  => 'sTRWFormReCaptchaV3SecretKey',
            'type'  => 'str',
            'value' => ''
        ],
        [
            'group'       => 'trwformrecaptchav3googleapi',
            'name'        => 'sTRWFormReCaptchaV3Score',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0'
        ]
    ]
];
