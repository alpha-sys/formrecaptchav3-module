[{if $oView|method_exists:'getFrontendControllerWithReCaptchaV3' && $module_var=='aTRWFormReCaptchaV3UseMessageForFrontendController'}]
    [{assign var="aForms" value=$oView->getFrontendControllerWithReCaptchaV3()}]
    <input type="hidden" name="confarrs[[{$module_var}]][]" value="" />
    <select name="confarrs[[{$module_var}]][]" multiple style="width: 210px;">
        [{foreach from=$aForms key=sKey item=oForm}]
            <option value="[{$oForm->name}]" [{if $oForm->selected}]selected[{/if}]>[{'SHOP_MODULE_Description'|cat:$oForm->name|oxmultilangassign}]</option>
        [{/foreach}]
    </select>
[{else}]
    [{$smarty.block.parent}]
[{/if}]