[{$smarty.block.parent}]
[{assign var="oConfig" value=$oViewConf->getConfig()}]
[{assign var="sReCaptchaV3WebsiteKey" value=$oConfig->getConfigParam('sTRWFormReCaptchaV3WebsiteKey')}]
[{oxscript include="https://www.google.com/recaptcha/api.js?render="|cat:$sReCaptchaV3WebsiteKey priority=5}]
[{capture assign="sGoogleRecaptchaV3"}]
    [{strip}]
        grecaptcha.ready(function () {
            grecaptcha.execute('[{$sReCaptchaV3WebsiteKey}]', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    [{/strip}]
[{/capture}]
[{oxscript add=$sGoogleRecaptchaV3}]
<input type="hidden" name="recaptcha_response" id="recaptchaResponse" />