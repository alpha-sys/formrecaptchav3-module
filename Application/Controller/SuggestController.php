<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller;

use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* suggest class
*
* @mixin \OxidEsales\Eshop\Application\Controller\SuggestController
*/
class SuggestController extends SuggestController_parent
{
    /**
    * Sends product suggestion mail and returns a URL according to
    * URL formatting rules.
    *
    * Template variables:
    * <b>editval</b>, <b>error</b>
    *
    * @return null
    */
    public function send()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            return null;
        }
        return parent::send();
    }
}
