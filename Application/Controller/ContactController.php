<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller;

use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* contact class
*
* @mixin \OxidEsales\Eshop\Application\Controller\ContactController
*/
class ContactController extends ContactController_parent
{
    /**
    * Composes and sends user written message, returns false if some parameters
    * are missing.
    *
    * @return bool
    */
    public function send()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            return false;
        }
        return parent::send();
    }
}
