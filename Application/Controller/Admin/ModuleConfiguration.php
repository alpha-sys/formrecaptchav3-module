<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* ModuleConfiguration class
*
* @mixin \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
*/
class ModuleConfiguration extends ModuleConfiguration_parent
{
    /**
    * Template Getter. List of Frontend Controller with ReCaptchaV3
    *
    * @return array
    */
    public function getFrontendControllerWithReCaptchaV3()
    {
        $aResult = [];

        // FrontendController List
        $aSelectedFrontendController = ToolsConfig::getOptionsArray('aTRWFormReCaptchaV3UseMessageForFrontendController');
        $aFrontendController = FormReCaptchaV3Helper::getFrontendControllerWithReCaptchaV3();
        foreach ($aFrontendController as $sFrontendController) {
            $oFrontendController = new \stdClass();
            $oFrontendController->name = $sFrontendController;
            $oFrontendController->selected = in_array($oFrontendController->name, $aSelectedFrontendController);
            $aResult[] = clone $oFrontendController;
        }
        return $aResult;
    }
}
