<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller;

use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* article details class
*
* @mixin \OxidEsales\Eshop\Application\Controller\ArticleDetailsController
*/
class ArticleDetailsController extends ArticleDetailsController_parent
{
    /**
    * Validates email address.
    * If email address is OK - creates price alarm object and saves it (oxPriceAlarm::save()).
    * If email is wrong - returns false.
    * Sends price alarm notification mail to shop owner.
    *
    * @return null
    */
    public function addMe()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            $this->_iPriceAlarmStatus = 0;

            return null;
        }
        return parent::addMe();
    }
}
