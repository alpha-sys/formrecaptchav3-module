<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2020 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\FormReCaptchaV3Module\Application\Component;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
* User object manager.
*
* @mixin \OxidEsales\Eshop\Application\Component\UserComponent
*/
class UserComponent extends UserComponent_parent
{
    /**
    * First test if all required fields were filled, then performed
    * additional checking oxcmp_user::CheckValues(). If no errors
    * occured - trying to create new user (\OxidEsales\Eshop\Application\Model\User::CreateUser()),
    * logging him to shop (\OxidEsales\Eshop\Application\Model\User::Login() if user has entered password).
    * If \OxidEsales\Eshop\Application\Model\User::CreateUser() returns false - this means user is
    * already created - we only logging him to shop (oxcmp_user::Login()).
    * If there is any error with missing data - function will return
    * false and set error code (oxcmp_user::iError). If user was
    * created successfully - will return "payment" to redirect to
    * payment interface.
    *
    * Template variables:
    * <b>usr_err</b>
    *
    * Session variables:
    * <b>usr_err</b>, <b>usr</b>
    *
    * @return  mixed redirection string or true if successful, false otherwise
    */
    public function createUser()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new \ReflectionClass($this))->getShortName())) {
            return false;
        }

        return parent::createUser();
    }
}
