OXID6 Google ReCaptcha V3 Protection Module
======

![vendor-logo the-real-world.de](picture.png)

### Features

* Add a hidden Google Recaptcha V3 to sensitive forms
* Forms: contact, login, register, account, invite, pricealarm, articlesuggest, newsletter

### Themeing

* 100% Compatible for WAVE or FLOW Standard-Theme

### Options in Module-Backend

* activate the Module
* Optional: the hidden individual varibales
* Optional: throw message on error in frontend
* Optional: throw message on "fallen in Google Recaptcha V3" in errorlog
* Optional: throw message on "use form" in errorlog
* Optional: collect the IP

### Module installation via composer

In order to install the module via composer run one of the following commands in commandline in your shop base directory
(where the shop's composer.json file resides).
* **composer require therealworld/ formrecaptchav3-module** to install the actual version compatible with OXID6

## Bugs and Issues

If you experience any bugs or issues, please report them in on https://bitbucket.org/therealworld/formrecaptchav3-module/issues.

